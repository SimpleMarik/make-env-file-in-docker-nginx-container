FROM nginx
COPY default.conf /etc/nginx/conf.d/default.conf
COPY ./dist /usr/share/nginx/html
COPY ./envScript.sh /usr/share/nginx/html
HEALTHCHECK --interval=30s --timeout=3s --start-period=30s \
  CMD curl -X 'GET' 'http://localhost' -H 'accept: */*' --fail || exit 1
CMD /bin/bash /usr/share/nginx/html/envScript.sh && nginx -g 'daemon off;'
